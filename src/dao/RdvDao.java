/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Rdv;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drissa
 */
public class RdvDao extends Dao<Rdv> {

    @Override
    public Rdv rechercher(int id) {
        Rdv r = new Rdv();
        try {

            ResultSet resultat = connect.createStatement().executeQuery("SELECT * FROM rdv WHERE idr =" + id);
            if (resultat.next()) {
                r.setIdr(resultat.getInt("idr"));
                r.setIdp(resultat.getInt("idp"));
                r.setDate_rdv(resultat.getDate("date_rdv"));
                r.setHeure_rdv(resultat.getString("heure_rdv"));
                r.setSalle(resultat.getString("salle"));
                r.setDocteur(resultat.getString("docteur"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RdvDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return r;
    }

    @Override
    public void ajouter(Rdv r) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO rdv (idp,date_rdv ,heure_rdv, salle, docteur) VALUES (?,?,?,?,? )");
            prepare.setInt(1, r.getIdp());
            prepare.setString(2, amj.format(r.getDate_rdv()));
            prepare.setString(3, r.getHeure_rdv());
            prepare.setString(4, r.getSalle());
            prepare.setString(5, r.getDocteur());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(RdvDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Rdv r, int id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE rdv SET idp=?,date_rdv=? ,heure_rdv=? ,salle=? ,docteur=? WHERE idr=" + id);
            prepare.setInt(1, r.getIdp());
            prepare.setString(2, amj.format(r.getDate_rdv()));
            prepare.setString(3, r.getHeure_rdv());
            prepare.setString(4, r.getSalle());
            prepare.setString(5, r.getDocteur());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(RdvDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM rdv WHERE idr =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RdvDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Rdv> afficher() {
        List<Rdv> list = new ArrayList();
        Rdv r = new Rdv();
        try {
            ResultSet resultat = this.connect.createStatement().executeQuery("SELECT * FROM rdv");
            while (resultat.next()) {
                r.setIdr(resultat.getInt("idr"));
                r.setIdp(resultat.getInt("idp"));
                r.setDate_rdv(resultat.getDate("date_rdv"));
                r.setHeure_rdv(resultat.getString("heure_rdv"));
                r.setSalle(resultat.getString("salle"));
                r.setDocteur(resultat.getString("docteur"));
                list.add(r);
                r = new Rdv();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RdvDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public List<Rdv> afficher2(String identifiant) {
       
         List<Rdv> list = new ArrayList();
        Rdv of = new Rdv();
        try {
           ResultSet resultat = this.connect.createStatement().executeQuery("SELECT * FROM rdv WHERE idp="+identifiant);
            while(resultat.next()){
            of.setIdr(resultat.getInt("idr"));
             of.setIdp(resultat.getInt("idp"));
            of.setDate_rdv(resultat.getDate("date_rdv"));
            of.setHeure_rdv(resultat.getString("heure_rdv"));
            of.setSalle(resultat.getString("salle"));
            of.setDocteur(resultat.getString("docteur"));
            list.add(of);
            of = new Rdv();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(RdvDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }
}
