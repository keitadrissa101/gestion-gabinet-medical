/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Drissa
 */
public abstract class Dao <T> {
    public Connection connect = ConnectDB.getInstance();
    public Dao(){}
        public abstract T rechercher(int id);
        public abstract void ajouter(T obj);
        public abstract void modifier (T obj, int id);
        public abstract void supprimer(int id);
        public abstract List<T> afficher();
    }