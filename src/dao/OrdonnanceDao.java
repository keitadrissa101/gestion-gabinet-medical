/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Ordonnance;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drissa
 */
public class OrdonnanceDao extends Dao<Ordonnance> {

    @Override
    public Ordonnance rechercher(int id) {
        Ordonnance o = new Ordonnance();
        try {

            ResultSet resultat = connect.createStatement().executeQuery("SELECT * FROM ordonnance WHERE ido =" + id);
            if (resultat.next()) {
                o.setIdo(resultat.getInt("ido"));
                o.setNomP(resultat.getString("nomp"));
                o.setPrenomP(resultat.getString("prenomp"));
                o.setNom_Medicament(resultat.getString("nom_medicament"));
                o.setDosage_Medicament(resultat.getString("dosage_medicament"));
                o.setNumf(resultat.getInt("numf"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(OrdonnanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }

    @Override
    public void ajouter(Ordonnance o) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO ordonnance (nomp, prenomp, nom_medicament, dosage_medicament, numf) VALUES (?,?,?,?,? )");
            prepare.setString(1, o.getNomP());
            prepare.setString(2, o.getPrenomP());
            prepare.setString(3, o.getNom_Medicament());
            prepare.setString(4, o.getDosage_Medicament());
            prepare.setInt(5, o.getNumf());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(OrdonnanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Ordonnance o, int id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE ordonnance SET nomp=?, prenomp=?, nom_medicament=?, dosage_medicament=?, numf=? WHERE ido="+ id);
            prepare.setString(1, o.getNomP());
            prepare.setString(2, o.getPrenomP());
            prepare.setString(3, o.getNom_Medicament());
            prepare.setString(4, o.getDosage_Medicament());
            prepare.setInt(5, o.getNumf());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrdonnanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM ordonnance WHERE ido =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrdonnanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Ordonnance> afficher() {
        List<Ordonnance> list = new ArrayList();
        Ordonnance o = new Ordonnance();
        try {
            ResultSet resultat = this.connect.createStatement().executeQuery("SELECT * FROM ordonnance");
            while (resultat.next()) {
                o.setIdo(resultat.getInt("ido"));
                o.setNomP(resultat.getString("nomp"));
                o.setPrenomP(resultat.getString("prenomp"));
                o.setNom_Medicament(resultat.getString("nom_medicament"));
                o.setDosage_Medicament(resultat.getString("dosage_medicament"));
                
                list.add(o);
                o = new Ordonnance();
            }

        } catch (SQLException ex) {
            Logger.getLogger(OrdonnanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
