/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Utilisateur;
import form.UtilisateurForm;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drissa
 */
public class UtilisateurDao extends Dao<Utilisateur> {

    @Override
    public Utilisateur rechercher(int id) {
        ResultSet result = null;
        Utilisateur u = new Utilisateur();
        try {

            result = this.connect.createStatement().executeQuery("SELECT * FROM utilisation WHERE idu =" + id);
            if (result.next()) {
                u.setIdu(result.getInt("idu"));
                u.setNom(result.getString("nom"));
                u.setPrenom(result.getString("prenom"));
                u.setLogin(result.getString("login"));
                u.setPassword(result.getString("password"));
                u.setSpecialite(result.getString("specialite"));
                u.setProfil(result.getString("profil"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;
    }

    @Override
    public void ajouter(Utilisateur u) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO utilisation(nom, prenom, login, password, profil ,specialite) VALUES (?,?,?,?,?,?)");
            prepare.setString(1, u.getNom());
            prepare.setString(2, u.getPrenom());
            prepare.setString(3, u.getLogin());
            prepare.setString(4, u.getPassword());
            prepare.setString(5, u.getProfil());
              prepare.setString(6, u.getSpecialite());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Utilisateur u, int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE utilisation SET nom = ? , prenom = ?, login = ?, password = ?, profil = ? ,specialite = ? WHERE idu =" + id);
            prepare.setString(1, u.getNom());
            prepare.setString(2, u.getPrenom());
            prepare.setString(3, u.getLogin());
            prepare.setString(4, u.getPassword());
            prepare.setString(5, u.getProfil());
            prepare.setString(6, u.getSpecialite());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM utilisateur WHERE idu =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Utilisateur> afficher() {
        List<Utilisateur> user = new ArrayList();
        Utilisateur obj = new Utilisateur();
        ResultSet result;
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM utilisation");
            while (result.next()) {
                obj.setIdu(result.getInt("idu"));
                obj.setNom(result.getString("nom"));
                obj.setPrenom(result.getString("prenom"));
                obj.setLogin(result.getString("login"));
                obj.setPassword(result.getString("password"));
                obj.setProfil(result.getString("profil"));
                obj.setSpecialite(result.getString("specialite"));
                user.add(obj);
                obj = new Utilisateur();
            }

        } catch (SQLException ex) {
            Logger.getLogger(UtilisateurDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

}
