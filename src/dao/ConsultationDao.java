/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Consultation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drissa
 */
public class ConsultationDao extends Dao<Consultation> {

    @Override
    public Consultation rechercher(int id) {
        Consultation co = new Consultation();
        try {

            ResultSet resultat = connect.createStatement().executeQuery("SELECT * FROM consultation WHERE idc =" + id);
            if (resultat.next()) {
                co.setIdc(resultat.getInt("idc"));
                co.setIdp(resultat.getInt("idp"));
                co.setIdu(resultat.getInt("idu"));
                co.setNomP(resultat.getString("nomp"));
                co.setPrenomP(resultat.getString("prenomp"));
                co.setMotif(resultat.getString("motif"));
                co.setTaille(resultat.getString("taille"));
                co.setPoid(resultat.getString("poid"));
                co.setTension(resultat.getString("tension"));
                co.setTemperature(resultat.getString("temperature"));
                co.setExamen(resultat.getString("examen"));
                co.setConclusion(resultat.getString("conclusion"));
                co.setDate(resultat.getDate("date"));
                co.setPrix(resultat.getInt("prix"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return co;
    }

    @Override
    public void ajouter(Consultation co) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO consultation (idp,idu, nomp,prenomp,motif ,taille, poid, tension, temperature,examen,conclusion,date,prix) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,? )");
            prepare.setInt(1, co.getIdp());
            prepare.setInt(2, co.getIdu());
            prepare.setString(3, co.getNomP());
            prepare.setString(4, co.getPrenomP());
            prepare.setString(5, co.getMotif());
            prepare.setString(6, co.getTaille());
            prepare.setString(7, co.getPoid());
            prepare.setString(8, co.getTension());
            prepare.setString(9, co.getTemperature());
            prepare.setString(10, co.getExamen());
            prepare.setString(11, co.getConclusion());
            prepare.setString(12, amj.format(co.getDate()));
            prepare.setInt(13, co.getPrix());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ConsultationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Consultation co, int id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE consultation SET  idp=?,idu=?, nomp=?,prenomp=?,motif=? ,taille=?, poid=?, tension=?, temperature=?,examen=?,conclusion=?,date=?,heure=? WHERE idc=" + id);
            prepare.setInt(1, co.getIdp());
            prepare.setInt(2, co.getIdu());
            prepare.setString(3, co.getNomP());
            prepare.setString(4, co.getPrenomP());
            prepare.setString(5, co.getMotif());
            prepare.setString(6, co.getTaille());
            prepare.setString(7, co.getPoid());
            prepare.setString(8, co.getTension());
            prepare.setString(9, co.getTemperature());
            prepare.setString(10, co.getExamen());
            prepare.setString(11, co.getConclusion());
            prepare.setString(12, amj.format(co.getDate()));
            prepare.setInt(13, co.getPrix());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ConsultationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM consultation WHERE idc =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConsultationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Consultation> afficher() {
        List<Consultation> list = new ArrayList();
        Consultation co = new Consultation();
        try {
            ResultSet resultat = this.connect.createStatement().executeQuery("SELECT * FROM consultation");
            while (resultat.next()) {
                co.setIdc(resultat.getInt("idc"));
                co.setIdp(resultat.getInt("idp"));
                co.setIdu(resultat.getInt("idu"));
                co.setNomP(resultat.getString("nomp"));
                co.setPrenomP(resultat.getString("prenomp"));
                co.setMotif(resultat.getString("motif"));
                co.setTaille(resultat.getString("taille"));
                co.setPoid(resultat.getString("poid"));
                co.setTension(resultat.getString("tension"));
                co.setTemperature(resultat.getString("temperature"));
                co.setExamen(resultat.getString("examen"));
                co.setConclusion(resultat.getString("conclusion"));
                co.setDate(resultat.getDate("date"));
                co.setPrix(resultat.getInt("prix"));
                list.add(co);
                co = new Consultation();
            }

        } catch (SQLException ex) {
            Logger.getLogger(ConsultationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public int montant(){
        int montant = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT SUM(prix) as montant FROM consultation");
            if(result.next() ){
              montant = result.getInt("montant");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return montant;
    }
     
     public int nombre(){
        int nombre = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT count(idc) as nombre FROM consultation");
            if(result.next() ){
              nombre = result.getInt("nombre");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return nombre;
    }
}
