/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Effectueanalysedetail;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bureau
 */
public class EffectueanalysedetailDao extends Dao <Effectueanalysedetail> {

    @Override
    public Effectueanalysedetail rechercher(int id) {
       ResultSet result = null;
        Effectueanalysedetail c = new Effectueanalysedetail();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM effectueanalysedetail WHERE id ="+id);
            if(result.next() ){
                c.setId(result.getInt("id"));
                c.setId_effan(result.getInt("id_effan"));
                c.setDesignation(result.getString("designation"));
                c.setPrix(result.getInt("prix"));
                c.setDated(result.getDate("dated"));
                c.setNum(result.getInt("num"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return c;
    }

    @Override
    public void ajouter(Effectueanalysedetail c) {
        try {
            SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO effectueanalysedetail(id_effan, designation, prix, dated, num) VALUES (?,?,?,?,?)");
            prepare.setInt(1, c.getId_effan());
            prepare.setString(2, c.getDesignation());
            prepare.setInt(3, c.getPrix());
            prepare.setString(4, amj.format(c.getDated()));
            prepare.setInt(5, c.getNum());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Effectueanalysedetail c, int id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE effectueanalysedetail SET designation = ?, prix = ? WHERE id ="+id);
            prepare.setInt(1, c.getId_effan());
            prepare.setString(2, c.getDesignation());
            prepare.setInt(3, c.getPrix());
            prepare.setString(4, amj.format(c.getDated()));
            prepare.setInt(5, c.getNum());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM effectueanalysedetail WHERE id ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Effectueanalysedetail> afficher() {
        List<Effectueanalysedetail> cat = new ArrayList();
        Effectueanalysedetail obj = new Effectueanalysedetail();
        ResultSet result;
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM effectueanalysedetail");
            while(result.next()){
            obj.setId(result.getInt("id"));
            obj.setId_effan(result.getInt("id_effan"));
            obj.setDesignation(result.getString("designation"));
            obj.setPrix(result.getInt("prix"));
            cat.add(obj);
            obj = new Effectueanalysedetail();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cat;
    }
     
     
     public int montant(){
        int montant = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT SUM(prix) as montant FROM effectueanalysedetail");
            if(result.next() ){
              montant = result.getInt("montant");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return montant;
    }
     
     public int nombre(){
        int nombre = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT count(id_effan) as nombre FROM effectueanalysedetail");
            if(result.next() ){
              nombre = result.getInt("nombre");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return nombre;
    }
     
      public int montantf(int numc){
        int montant = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT SUM(quantite*pu) as montant FROM effectueanalysedetail WHERE numf ="+numc);
            if(result.next() ){
              montant = result.getInt("montant");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return montant;
    }
    
      public int montantjrs(Date jour){
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        int montant = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT SUM(quantite*pu) as montant FROM effectueanalysedetail where dated='"+amj.format(jour)+"' ");
            if(result.next() ){
              montant = result.getInt("montant");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return montant;
    }
     
     public int nombrejrs(Date jour){
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        int nombre = 0;
        ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT count(id) as nombrej FROM effectueanalysedetail where dated='"+amj.format(jour)+"'");
            if(result.next() ){
              nombre = result.getInt("nombrej");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalysedetailDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return nombre;
    }
}
