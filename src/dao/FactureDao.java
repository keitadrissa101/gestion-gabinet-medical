/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Facture;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drissa
 */
public class FactureDao extends Dao<Facture> {

    @Override
    public Facture rechercher(int id) {
        ResultSet result = null;
        Facture f = new Facture();
        try {

            result = this.connect.createStatement().executeQuery("SELECT * FROM recu WHERE idc =" + id);
            if (result.next()) {
                f.setIdc(result.getInt("idc"));
                f.setNumero_consu(result.getString("numero_consu"));
                f.setPrix(result.getInt("prix"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FactureDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return f;
    }

    @Override
    public void ajouter(Facture f) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO recu (numero_consu, prix) VALUES (?,?)");
            prepare.setString(1, f.getNumero_consu());
            prepare.setInt(2, f.getPrix());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FactureDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Facture f, int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE recu SET numero_consu=?, prix=? WHERE idc" + id);
            prepare.setString(1, f.getNumero_consu());
            prepare.setInt(2, f.getPrix());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FactureDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM recu WHERE id =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FactureDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Facture> afficher() {
        List<Facture> fact = new ArrayList();
        Facture obj = new Facture();
        ResultSet result;
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM recu");
            while (result.next()) {
                obj.setIdc(result.getInt("idc"));
                obj.setNumero_consu(result.getString("numero_consu"));
                obj.setPrix(result.getInt("prix"));

                fact.add(obj);
                obj = new Facture();
            }

        } catch (SQLException ex) {
            Logger.getLogger(FactureDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fact;
    }
}
