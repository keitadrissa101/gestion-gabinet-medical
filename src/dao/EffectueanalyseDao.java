/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Effectueanalyse;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bureau
 */
public class EffectueanalyseDao extends Dao <Effectueanalyse> {

    
    public int lastid(){
        int max = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT MAX(ido) as maximum FROM effectueanalyse");
            if(result.next() ){
              max = result.getInt("maximum");   
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return max;
    }
    
    public Date lastdate(){
        Date d = null;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT ido, dateefa FROM effectueanalyse  ORDER BY id Desc limit 0,1");
            if(result.next() ){
              d = result.getDate("dateefa");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return d;
    }

    @Override
    public Effectueanalyse rechercher(int id) {
        ResultSet result = null;
        Effectueanalyse c = new Effectueanalyse();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM effectueanalyse WHERE id ="+id);
            if(result.next() ){
                c.setId(result.getInt("id"));
                c.setId_utilisateur(result.getInt("id_utilisateur"));
                c.setDateefa(result.getDate("dateefa"));
                c.setTotal(result.getInt("total"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return c;
    }

    @Override
    public void ajouter(Effectueanalyse c) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO effedtueanalyse(id_utilisateur, dateefa, total) VALUES (?,?,?)");
            prepare.setInt(1, c.getId_utilisateur());
            prepare.setString(2, amj.format(c.getDateefa()));
            prepare.setInt(3, c.getTotal());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Effectueanalyse c, int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE effectueanalyse SET total = ? WHERE id ="+id);
            prepare.setInt(1, c.getTotal());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
       try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM effectueanalyse WHERE id ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Effectueanalyse> afficher() {
        List<Effectueanalyse> cat = new ArrayList();
        Effectueanalyse obj = new Effectueanalyse();
        ResultSet result;
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM effectueanalyse");
            while(result.next()){
            obj.setId(result.getInt("id"));
            obj.setId_utilisateur(result.getInt("id_utilisateur"));
            obj.setDateefa(result.getDate("dateefa"));
            obj.setTotal(result.getInt("total"));
            cat.add(obj);
            obj = new Effectueanalyse();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cat;
    }
    
    }