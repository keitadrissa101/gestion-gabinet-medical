/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Analyse;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bureau
 */
public class AnalyseDao extends Dao<Analyse> {

    @Override
    public Analyse rechercher(int id) {
        ResultSet result = null;
        Analyse c = new Analyse();
        try {

            result = this.connect.createStatement().executeQuery("SELECT * FROM analyse WHERE id =" + id);
            if (result.next()) {
                c.setId(result.getInt("id"));
                c.setDescription(result.getString("description"));
                c.setPrix(result.getInt("prix"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AnalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }

    @Override
    public void ajouter(Analyse c) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO analyse(description, prix) VALUES (?,?)");
            prepare.setString(1, c.getDescription());
            prepare.setInt(2, c.getPrix());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Analyse c, int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE analyse SET description = ?, prix = ? WHERE id =" + id);
            prepare.setString(1, c.getDescription());
            prepare.setInt(2, c.getPrix());
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM analyse WHERE id =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Analyse> afficher() {
        List<Analyse> cat = new ArrayList();
        Analyse obj = new Analyse();
        ResultSet result;
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM analyse");
            while (result.next()) {
                obj.setId(result.getInt("id"));
                obj.setDescription(result.getString("description"));
                obj.setPrix(result.getInt("prix"));
                cat.add(obj);
                obj = new Analyse();
            }

        } catch (SQLException ex) {
            Logger.getLogger(AnalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cat;
    }

    public int lastidp() {
        int max = 0;
        ResultSet result = null;
        try {

            result = this.connect.createStatement().executeQuery("SELECT MAX(idp) as maximum FROM analyse");
            if (result.next()) {
                max = result.getInt("maximum");

            }
        } catch (SQLException ex) {
            Logger.getLogger(EffectueanalyseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return max;
    }

}
