/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Factureo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bureau
 */
public class FactureoDao  extends Dao <Factureo> {

 
    
 public int numfct(){
     int max = 0;
     ResultSet result = null;
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT MAX(numc) as maximum FROM factureo");
            if(result.next() ){
              max = result.getInt("maximum");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(FactureoDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return max+1;
 }   

    @Override
    public Factureo rechercher(int id) {
        ResultSet result = null;
        Factureo c = new Factureo();
        try {
            
            result = this.connect.createStatement().executeQuery("SELECT * FROM factureo WHERE numc ="+id);
            if(result.next() ){
                c.setNumc(result.getInt("numc"));
                c.setDatef(result.getDate("datef"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(FactureoDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return c;

    }

    @Override
    public void ajouter(Factureo f) {
       SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO factureo(numc, datef) VALUES (?,?)");
            prepare.setInt(1, f.getNumc());
            prepare.setString(2, amj.format(f.getDatef()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FactureoDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public void modifier(Factureo f, int id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE factureo SET numc=?,datef=? WHERE numc ="+id);
            prepare.setInt(1, f.getNumc());
            prepare.setString(2, amj.format(f.getDatef()));
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FactureoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM factureo WHERE num ="+id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FactureoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public List<Factureo> afficher() {
        SimpleDateFormat amj = new SimpleDateFormat("dd-MM-yyyy");
        List<Factureo> cmd = new ArrayList();
        Factureo obj = new Factureo();
        ResultSet result;
        try {
            result = this.connect.createStatement().executeQuery("SELECT * FROM factureo");
            while(result.next()){
            obj.setNumc(result.getInt("numc"));
            obj.setDatef(result.getDate("datef"));
            cmd.add(obj);
            obj = new Factureo();   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(FactureoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cmd;
    }
    
    
    
}
