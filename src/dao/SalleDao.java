/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Salle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drissa
 */
public class SalleDao extends Dao<Salle> {

    @Override
    public Salle rechercher(int id) {
        Salle s = new Salle();
        try {

            ResultSet resultat = connect.createStatement().executeQuery("SELECT * FROM salle WHERE ids =" + id);
            if (resultat.next()) {
                s.setIds(resultat.getInt("ids"));
                s.setNom_salle(resultat.getString("nom_salle"));
         
            }
        } catch (SQLException ex) {
            Logger.getLogger(SalleDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;
    }

    @Override
    public void ajouter(Salle s) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO salle (nom_salle) VALUES (? )");
            prepare.setString(1, s.getNom_salle());
         
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(SalleDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Salle s, int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE salle SET nom_salle=? WHERE ids=" + id);
            prepare.setString(1, s.getNom_salle());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(SalleDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM salle WHERE ids =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SalleDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Salle> afficher() {
        List<Salle> list = new ArrayList();
        Salle s = new Salle();
        try {
            ResultSet resultat = this.connect.createStatement().executeQuery("SELECT * FROM salle");
            while (resultat.next()) {
                s.setIds(resultat.getInt("ids"));
                s.setNom_salle(resultat.getString("nom_salle"));
               
                list.add(s);
                s = new Salle();
            }

        } catch (SQLException ex) {
            Logger.getLogger(SalleDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
