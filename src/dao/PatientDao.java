/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import classes.Patient;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Drissa
 */
public class PatientDao extends Dao<Patient> {

    @Override
    public Patient rechercher(int id) {
        Patient p = new Patient();
        try {

            ResultSet resultat = connect.createStatement().executeQuery("SELECT * FROM patient WHERE idp =" + id);
            if (resultat.next()) {
                p.setIdP(resultat.getInt("idp"));
                p.setNomP(resultat.getString("nomp"));
                p.setMatricule_p(resultat.getString("matricule_p"));
                p.setPrenomP(resultat.getString("prenomp"));
                p.setDate_Naissance(resultat.getDate("date_naissance"));
                p.setAntecedents(resultat.getString("antecedents"));
                p.setAdresse(resultat.getString("adresse"));
                p.setSexe(resultat.getString("sexe"));
                p.setGroupe_Sanguine(resultat.getString("groupe_saguine"));
                p.setTelephone(resultat.getString("telephone"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    @Override
    public void ajouter(Patient p) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("INSERT INTO patient ( matricule_p,nomp,prenomp,date_Naissance ,antecedents, adresse, sexe, groupe_Saguine,telephone) VALUES (?,?,?,?,?,?,?,?,?)");
            prepare.setString(1, p.getMatricule_p());
            prepare.setString(2, p.getNomP());
            prepare.setString(3, p.getPrenomP());
            prepare.setString(4, amj.format(p.getDate_Naissance()));
            prepare.setString(5, p.getAntecedents());
            prepare.setString(6, p.getAdresse());
            prepare.setString(7, p.getSexe());
            prepare.setString(8, p.getGroupe_Sanguine());
            prepare.setString(9, p.getTelephone());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifier(Patient p, int id) {
        SimpleDateFormat amj = new SimpleDateFormat("yyyy-MM-dd");
        try {
            PreparedStatement prepare = this.connect.prepareStatement("UPDATE patient SET matricule_p=?,  nomp=?,prenomp=?,date_Naissance=? ,antecedents=?, adresse=?, sexe=?, groupe_Saguine=?,telephone=? WHERE idp=" + id);
            prepare.setString(1, p.getMatricule_p());
            prepare.setString(2, p.getNomP());
            prepare.setString(3, p.getPrenomP());
            prepare.setString(4, amj.format(p.getDate_Naissance()));
            prepare.setString(5, p.getAntecedents());
            prepare.setString(6, p.getAdresse());
            prepare.setString(7, p.getSexe());
            prepare.setString(8, p.getGroupe_Sanguine());
            prepare.setString(9, p.getTelephone());
            prepare.execute();
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimer(int id) {
        try {
            PreparedStatement prepare = this.connect.prepareStatement("DELETE FROM patient WHERE idp =" + id);
            prepare.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Patient> afficher() {
        List<Patient> list = new ArrayList();
        Patient p = new Patient();
        try {
            ResultSet resultat = this.connect.createStatement().executeQuery("SELECT * FROM patient order by idp desc");
            while (resultat.next()) {
                p.setIdP(resultat.getInt("idp"));
                p.setMatricule_p(resultat.getString("matricule_p"));
                p.setNomP(resultat.getString("nomp"));
                p.setPrenomP(resultat.getString("prenomp"));
                p.setDate_Naissance(resultat.getDate("date_Naissance"));
                p.setAntecedents(resultat.getString("antecedents"));
                p.setAdresse(resultat.getString("adresse"));
                p.setSexe(resultat.getString("adresse"));
                p.setGroupe_Sanguine(resultat.getString("groupe_saguine"));
                p.setTelephone(resultat.getString("telephone"));

                list.add(p);
                p = new Patient();
            }

        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
