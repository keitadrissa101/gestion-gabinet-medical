/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author Drissa
 */
public class Patient {
     private int idP;
     private String matricule_p;
    private String nomP;
    private String prenomP;
    private Date date_Naissance;
    private String antecedents;
    private String adresse;
    private String sexe;
    private String groupe_Sanguine;
    private String telephone;

    public Patient() {
    }

    public Patient(int idP, String matricule_p, String nomP, String prenomP, Date date_Naissance, String antecedents, String adresse, String sexe, String groupe_Sanguine, String telephone) {
        this.idP = idP;
        this.matricule_p = matricule_p;
        this.nomP = nomP;
        this.prenomP = prenomP;
        this.date_Naissance = date_Naissance;
        this.antecedents = antecedents;
        this.adresse = adresse;
        this.sexe = sexe;
        this.groupe_Sanguine = groupe_Sanguine;
        this.telephone = telephone;
    }

    public Patient(String matricule_p, String nomP, String prenomP, Date date_Naissance, String antecedents, String adresse, String sexe, String groupe_Sanguine, String telephone) {
        this.matricule_p = matricule_p;
        this.nomP = nomP;
        this.prenomP = prenomP;
        this.date_Naissance = date_Naissance;
        this.antecedents = antecedents;
        this.adresse = adresse;
        this.sexe = sexe;
        this.groupe_Sanguine = groupe_Sanguine;
        this.telephone = telephone;
    }

    public int getIdP() {
        return idP;
    }

    public String getMatricule_p() {
        return matricule_p;
    }

    public String getNomP() {
        return nomP;
    }

    public String getPrenomP() {
        return prenomP;
    }

    public Date getDate_Naissance() {
        return date_Naissance;
    }

    public String getAntecedents() {
        return antecedents;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getSexe() {
        return sexe;
    }

    public String getGroupe_Sanguine() {
        return groupe_Sanguine;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public void setMatricule_p(String matricule_p) {
        this.matricule_p = matricule_p;
    }

    public void setNomP(String nomP) {
        this.nomP = nomP;
    }

    public void setPrenomP(String prenomP) {
        this.prenomP = prenomP;
    }

    public void setDate_Naissance(Date date_Naissance) {
        this.date_Naissance = date_Naissance;
    }

    public void setAntecedents(String antecedents) {
        this.antecedents = antecedents;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public void setGroupe_Sanguine(String groupe_Sanguine) {
        this.groupe_Sanguine = groupe_Sanguine;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

   
}
