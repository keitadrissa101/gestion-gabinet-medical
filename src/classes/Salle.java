/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Drissa
 */
public class Salle {

    private int ids;
    private String nom_salle;

    public Salle() {
    }

    public Salle(int ids, String nom_salle) {
        this.ids = ids;
        this.nom_salle = nom_salle;
    }

    public Salle(String nom_salle) {
        this.nom_salle = nom_salle;
    }

    public int getIds() {
        return ids;
    }

    public String getNom_salle() {
        return nom_salle;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public void setNom_salle(String nom_salle) {
        this.nom_salle = nom_salle;
    }

}
   