/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author bureau
 */
public class Analyse {
    private int id;
    private String description;
    private int prix;

    public Analyse() {
    }

    public Analyse(String description, int prix) {
        this.description = description;
        this.prix = prix;
    }

    public Analyse(int id, String description, int prix) {
        this.id = id;
        this.description = description;
        this.prix = prix;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String designation) {
        this.description = designation;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

   
    
}
