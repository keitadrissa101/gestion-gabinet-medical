/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author bureau
 */
public class Effectueanalyse {
    private int id;
    private int id_utilisateur;
    private Date dateefa;
    private int total;

    public Effectueanalyse() {
    }

    public Effectueanalyse(int id_utilisateur, Date dateefa, int total) {
        this.id_utilisateur = id_utilisateur;
        this.dateefa = dateefa;
        this.total = total;
    }

    public Effectueanalyse(int id, int id_utilisateur, Date dateefa, int total) {
        this.id = id;
        this.id_utilisateur = id_utilisateur;
        this.dateefa = dateefa;
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_utilisateur() {
        return id_utilisateur;
    }

    public void setId_utilisateur(int id_utilisateur) {
        this.id_utilisateur = id_utilisateur;
    }

    public Date getDateefa() {
        return dateefa;
    }

    public void setDateefa(Date dateefa) {
        this.dateefa = dateefa;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    
    
}
