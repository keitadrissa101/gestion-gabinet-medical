/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author Drissa
 */
public class Rdv {

    private int idr;
    private int idp;
    private Date date_rdv;
    private String heure_rdv;
    private String salle;
    private String docteur;

    public Rdv() {
    }

    public Rdv(int idr, int idp, Date date_rdv, String heure_rdv, String salle, String docteur) {
        this.idr = idr;
        this.idp = idp;
        this.date_rdv = date_rdv;
        this.heure_rdv = heure_rdv;
        this.salle = salle;
        this.docteur = docteur;
    }

    public Rdv(int idp, Date date_rdv, String heure_rdv, String salle, String docteur) {
        this.idp = idp;
        this.date_rdv = date_rdv;
        this.heure_rdv = heure_rdv;
        this.salle = salle;
        this.docteur = docteur;
    }

    public int getIdr() {
        return idr;
    }

    public int getIdp() {
        return idp;
    }

    public Date getDate_rdv() {
        return date_rdv;
    }

    public String getHeure_rdv() {
        return heure_rdv;
    }

    public String getSalle() {
        return salle;
    }

    public String getDocteur() {
        return docteur;
    }

    public void setIdr(int idr) {
        this.idr = idr;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public void setDate_rdv(Date date_rdv) {
        this.date_rdv = date_rdv;
    }

    public void setHeure_rdv(String heure_rdv) {
        this.heure_rdv = heure_rdv;
    }

    public void setSalle(String salle) {
        this.salle = salle;
    }

    public void setDocteur(String docteur) {
        this.docteur = docteur;
    }

}
