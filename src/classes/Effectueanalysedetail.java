/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author bureau
 */
public class Effectueanalysedetail {
    private int id;
    private int id_effan;
    private String designation;
    private int prix;
    private Date dated;
    private int num;

    public Effectueanalysedetail() {
    }

    public Effectueanalysedetail(int id_effan, String designation, int prix, Date dated, int num) {
        this.id_effan = id_effan;
        this.designation = designation;
        this.prix = prix;
        this.dated = dated;
        this.num = num;
    }

    public Effectueanalysedetail(int id, int id_effan, String designation, int prix, Date dated, int num) {
        this.id = id;
        this.id_effan = id_effan;
        this.designation = designation;
        this.prix = prix;
        this.dated = dated;
        this.num = num;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_effan() {
        return id_effan;
    }

    public void setId_effan(int id_effan) {
        this.id_effan = id_effan;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public Date getDated() {
        return dated;
    }

    public void setDated(Date dated) {
        this.dated = dated;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

   
}
