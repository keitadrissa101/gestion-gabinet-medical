/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Drissa
 */
public class Utilisateur {
   private int idu;
    private String nom;
    private String prenom;
    private String login;
    private String password;
    private String profil;
    private String specialite;

    public Utilisateur() {
    }

    public Utilisateur(String nom, String prenom, String login, String password, String profil, String specialite) {
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.profil = profil;
        this.specialite = specialite;
    }

    public Utilisateur(int idu, String nom, String prenom, String login, String password, String profil, String specialite) {
        this.idu = idu;
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.password = password;
        this.profil = profil;
        this.specialite = specialite;
    }

    public int getIdu() {
        return idu;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getProfil() {
        return profil;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setIdu(int idu) {
        this.idu = idu;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

   
}
