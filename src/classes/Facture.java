/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Drissa
 */
public class Facture {
      private int idc;
    private String numero_consu;
    private int prix;
    
    public Facture() {
    }

    public Facture(String numero_consu, int prix) {
        this.numero_consu = numero_consu;
        this.prix = prix;
    }

    public Facture(int idc, String numero_consu, int prix) {
        this.idc = idc;
        this.numero_consu = numero_consu;
        this.prix = prix;
    }

    public int getIdc() {
        return idc;
    }

    public void setIdc(int idc) {
        this.idc = idc;
    }

    public String getNumero_consu() {
        return numero_consu;
    }

    public void setNumero_consu(String numero_consu) {
        this.numero_consu = numero_consu;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

}