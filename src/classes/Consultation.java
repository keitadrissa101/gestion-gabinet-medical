/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author Drissa
 */
public class Consultation {
    
    private int idc;
    private int idp;
    private int idu;
    private String nomP;
    private String prenomP;
    private String motif;
    private String taille;
    private String poid;
    private String tension;
    private String temperature;
    private String examen;
    private String conclusion;
    private Date date;
    private int prix;

    public Consultation() {
    }

    public Consultation(int idp, int idu, String nomP, String prenomP, String motif, String taille, String poid, String tension, String temperature, String examen, String conclusion, Date date, int prix) {
        this.idp = idp;
        this.idu = idu;
        this.nomP = nomP;
        this.prenomP = prenomP;
        this.motif = motif;
        this.taille = taille;
        this.poid = poid;
        this.tension = tension;
        this.temperature = temperature;
        this.examen = examen;
        this.conclusion = conclusion;
        this.date = date;
        this.prix = prix;
    }

    public Consultation(int idc, int idp, int idu, String nomP, String prenomP, String motif, String taille, String poid, String tension, String temperature, String examen, String conclusion, Date date, int prix) {
        this.idc = idc;
        this.idp = idp;
        this.idu = idu;
        this.nomP = nomP;
        this.prenomP = prenomP;
        this.motif = motif;
        this.taille = taille;
        this.poid = poid;
        this.tension = tension;
        this.temperature = temperature;
        this.examen = examen;
        this.conclusion = conclusion;
        this.date = date;
        this.prix = prix;
    }

    public int getIdc() {
        return idc;
    }

    public int getIdp() {
        return idp;
    }

    public int getIdu() {
        return idu;
    }

    public String getNomP() {
        return nomP;
    }

    public String getPrenomP() {
        return prenomP;
    }

    public String getMotif() {
        return motif;
    }

    public String getTaille() {
        return taille;
    }

    public String getPoid() {
        return poid;
    }

    public String getTension() {
        return tension;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getExamen() {
        return examen;
    }

    public String getConclusion() {
        return conclusion;
    }

    public Date getDate() {
        return date;
    }

    public int getPrix() {
        return prix;
    }

    public void setIdc(int idc) {
        this.idc = idc;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public void setIdu(int idu) {
        this.idu = idu;
    }

    public void setNomP(String nomP) {
        this.nomP = nomP;
    }

    public void setPrenomP(String prenomP) {
        this.prenomP = prenomP;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public void setPoid(String poid) {
        this.poid = poid;
    }

    public void setTension(String tension) {
        this.tension = tension;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setExamen(String examen) {
        this.examen = examen;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

   
}
