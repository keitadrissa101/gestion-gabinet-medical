/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author bureau
 */
public class Factureo {
    private int numc;
    private Date datef;

    public Factureo() {
    }

    public Factureo(Date datef) {
        this.datef = datef;
    }

    public Factureo(int numc, Date datef) {
        this.numc = numc;
        this.datef = datef;
    }

    public int getNumc() {
        return numc;
    }

    public void setNumc(int numc) {
        this.numc = numc;
    }

    public Date getDatef() {
        return datef;
    }

    public void setDatef(Date datef) {
        this.datef = datef;
    }

    
}
