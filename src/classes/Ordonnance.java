/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Date;

/**
 *
 * @author Drissa
 */
public class Ordonnance {
    private int ido;
    private String nomP;
    private String prenomP;
    private String nom_Medicament;
    private String dosage_Medicament;
    private int numf;

    public Ordonnance() {
    }

    public Ordonnance(String nomP, String prenomP, String nom_Medicament, String dosage_Medicament, int numf) {
        this.nomP = nomP;
        this.prenomP = prenomP;
        this.nom_Medicament = nom_Medicament;
        this.dosage_Medicament = dosage_Medicament;
        this.numf = numf;
    }

    public Ordonnance(int ido, String nomP, String prenomP, String nom_Medicament, String dosage_Medicament, int numf) {
        this.ido = ido;
        this.nomP = nomP;
        this.prenomP = prenomP;
        this.nom_Medicament = nom_Medicament;
        this.dosage_Medicament = dosage_Medicament;
        this.numf = numf;
    }

    public int getIdo() {
        return ido;
    }

    public void setIdo(int ido) {
        this.ido = ido;
    }

    public String getNomP() {
        return nomP;
    }

    public void setNomP(String nomP) {
        this.nomP = nomP;
    }

    public String getPrenomP() {
        return prenomP;
    }

    public void setPrenomP(String prenomP) {
        this.prenomP = prenomP;
    }

    public String getNom_Medicament() {
        return nom_Medicament;
    }

    public void setNom_Medicament(String nom_Medicament) {
        this.nom_Medicament = nom_Medicament;
    }

    public String getDosage_Medicament() {
        return dosage_Medicament;
    }

    public void setDosage_Medicament(String dosage_Medicament) {
        this.dosage_Medicament = dosage_Medicament;
    }

    public int getNumf() {
        return numf;
    }

    public void setNumf(int numf) {
        this.numf = numf;
    }

}
